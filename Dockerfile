FROM alpine:3.6
RUN set -x
RUN apk upgrade --update-cache --available
RUN apk add --no-cache nginx git
WORKDIR /usr/share/nginx/
RUN git clone https://github.com/jordimurgo/votarem.git
RUN ln -s /usr/share/nginx/votarem/ /usr/share/nginx/html
RUN ln -s /usr/share/nginx/votarem/ /usr/share/nginx/html/doctubre
COPY nginx.conf /etc/nginx/nginx.conf
COPY nginx.vh.default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80 443
STOPSIGNAL SIGTERM
CMD ["nginx", "-g", "daemon off;"]
